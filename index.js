import server from './app';

server.start((err) => {
    if (err) {
        throw err;
    }
    console.log(`SERVER RODANDO EM: ${server.info.uri}`);
});

export default server;
