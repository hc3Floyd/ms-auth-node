import AuthService from './src/modules/auth/AuthService';

var plugins = [
    {
        register: require('hapi-auth-jwt')
    }
];

exports.register = (server, options ,next) => {
    
  server.register(plugins,(err) => {

      if(err) throw err;

      const authService = new AuthService();

      function validate(request, decodedToken, callback) {

        return authService.validateToken(request,decodedToken,callback);
        
      };

      server.auth.strategy('token', 'jwt', {
          key: process.env.SECRET,
          validateFunc: validate,
          verifyOptions: { algorithms: [ 'HS256' ] }  // only allow HS256 algorithm
      });

      next();
  })
}

exports.register.attributes = {
  name: 'authentication',
  version: '1.0.0'
}