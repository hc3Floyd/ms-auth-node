import jwt from 'jwt-simple';

class AbstractService {

    constructor(instanceModel, factory) {
        this.factory = factory;
        this.repository = instanceModel;
    };

    async listAll(reply) {
        const list = await this.repository.find(); 
        return reply(list);
    };

    async insert(request, reply) {
        const entity = this.factory.make(request);
        try {
            const response = await this.repository.save(entity);
            return reply(response);
        } catch (err) {
            return reply(err);
        }
    }

    async update(request, reply) {
        let entity = this.factory.make(request);
        entity = repository.findOne(entity.id);
        try {
            const response = await this.repository.merge(entity);
            return reply(response);
        } catch (err) {
            return reply(err);
        }
    }

    async findOne(request, reply) {
        let entity = this.factory.make(request);
        const response = await this.repository.findone(entity.id);
        return reply(response);
    }

    isAuthorized(request, type) {

        let isAuthorized = false;

        request.auth.credentials.listRoles.map(role => {
            if(role.access === type || role.access === 'admin') {
                isAuthorized = true;
            }
        });

        return isAuthorized;
    }

    isSameUser(request) {

        let isSameUser = false;

        const role = request.auth.credentials.role;

        if(role === 'admin') {

            isSameUser = true;

        } else {

            const token = request.headers.authorization.replace("Bearer ","");
            const id = request.auth.credentials.id;

            var decodedToken = jwt.decode(token, process.env.SECRET);

            if(decodedToken.id === id) {
                isSameUser = true;
            };

        };

        return isSameUser;
    }

    notAuthorized(res) {

        let message = {
            error: true,
            message: 'Usuário não tem autorização para acessar'
        };

        return res(message);

    };

    catchError(err,reply) {
        return null;
    }

}

export default AbstractService;