function make(request) {
    let object = {};

    for(let propertie in request.payload) {
        object[propertie] = request.payload[propertie];
    }

    return object;
};

const factory = {
    make
};

export default factory;