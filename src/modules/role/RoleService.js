import { getRepository } from "typeorm";
import { Role } from '../models/Role';
import AbstractService from '../AbstractService';
import roleFactory from './RoleFactory';

class RoleService extends AbstractService {
    
    constructor() {
        super(Role, roleFactory);
    }
}

export default RoleService;
