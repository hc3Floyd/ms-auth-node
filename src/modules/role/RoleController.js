import RoleService from './RoleService';

exports.register = function(server, options, next) {

    const service = new RoleService();

    server.route({
        method:'POST',
        path:'/create-role',
        config:{
            auth:'token',
            handler: (request, reply) => {

                if(service.isAuthorized(request,'admin')) {
                    service.insert(request,reply);
                } else {
                    service.notAuthorized(reply);
                }

            }
        }
    });

    return next();
}

exports.register.attributes = {
    name:'role'
}