import AccountService from './AccountService';

exports.register = function(server, options ,next) {

    const service = new AccountService();

    server.route({
        method:'POST',
        path:'/create-account',
        handler:(request, reply) => {
            service.insert(request, reply);
        }
    });

    server.route({
        method:'PUT',
        path:'/update-account',
        handler:(request, reply) => {
            if(service.isSameUser(request)) {
                service.update(request, reply);
            } else {
                reply({code:997, error:true, message:'erro no request'});
            }
        }
    });

    server.route({
        method:'GET',
        path:'/get-account',
        handler:(request, reply) => {
            if(service.isSameUser(request)) {
                service.findOne(request, reply);
            } else {
                reply({code:997, error:true, message:'erro no request'});
            }
        }
    });

    return next();
}

exports.register.attributes = {
    name: 'account'
};