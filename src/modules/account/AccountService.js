import { Account } from '../models/Account';
import AbstractService from '../AbstractService';
import accountFactory from './AccountFactory';
import { getRepository } from "typeorm";

class AccountService extends AbstractService {

    constructor() { 
        super(getRepository(Account), accountFactory);
    };

}

export default AccountService;