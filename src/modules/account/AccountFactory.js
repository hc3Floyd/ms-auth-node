import bcrypt from 'bcrypt';

const SALT_WORK_FACTOR = 10;

function makeSalt(password) {
    var salt = bcrypt.genSaltSync(SALT_WORK_FACTOR);
    var hash = bcrypt.hashSync(password, salt);
    return hash;
}

function make(request) {
    return {
        email: request.payload.email,
        password: makeSalt(request.payload.password)
    };
}

const factory = {
    make
}

export default factory;