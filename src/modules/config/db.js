import { createConnection } from "typeorm";

const db = createConnection();

export default db;

