import {EntitySchema} from "typeorm";

export const Account  = new EntitySchema({
    name:'account',
    columns: {
        id: {
            type:Number,
            primary:true,
            generated:true
        },
        isActive: {
            type:Boolean,
            default:false
        },
        email: {
            type:String,
            nullable:false
        },
        password: {
            type:String,
            nullable:false
        }
    },
    uniques:[{
        name:'unique_email',
        columns:['email']
    }],
    relations: {
        listRoles: {
            target:'role',
            type:'many-to-many',
            inverseSide:'in'
        }
    }
})