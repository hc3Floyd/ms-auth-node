import { EntitySchema } from 'typeorm';

export const Role = new EntitySchema({
    name:'role',
    columns: {
        id: {
            type:Number,
            primary:true,
            generated:true
        },
        isActive: {
            type:Boolean,
            default:true
        },
        description: {
            type:String,
            nullable:false
        },
        access: {
            type:String,
            default:'basic',
            nullable:false
        }
    },
    uniques:[{
        name:'unique_access',
        columns:['access']
    }],
    relations: {
        in: {
            target:'account',
            type:'many-to-many',
            inverseSide:"listRoles",
            joinTable: true,
        }
    }
})