import AuthService from './AuthService';

exports.register = function(server, options, next) {

    const service = new AuthService(server.app.db);

    server.route({
        method:'POST',
        path:'/login',
        handler:(request, reply) => {
            service.createToken(request, reply);
        }
    });

    server.route({
        method:'POST',
        path:'/logout',
        handler:(request, reply) => {
            //action.deleteToken(request,reply,service);
        }
    });

    return next();
};

exports.register.attributes = {
    name: 'routes-auth'
};