import bcrypt from 'bcrypt';
import jwt from 'jwt-simple';

function comparePassword(candidate, passwordDB) {
    return bcrypt.compareSync(candidate, passwordDB);
}

function make(request) {
    return {
        email: request.payload.email,
        password: request.payload.password
    };
}

function check(userInDb, userInPost, reply) {
    const isMath = comparePassword(userInPost.password, userInDb.password);
    if(isMath) {

        const payload = {
            id:userInDb.id,
            role:userInDb.role,
            isActive:userInDb.isActive
        };

        let token = {token:jwt.encode(payload,process.env.SECRET)}
        
        reply(token);

    } else {
        reply({code:998,error:true,message:'erro ao realizar login'})
    }
}

const factory = {
    make,
    check
}

export default factory;