import { Account } from '../models/Account';
import authFactory from './AuthFactory';
import {getRepository} from "typeorm";

class AuthService {

  constructor() {

  }

  async createToken(request, reply) {

    const repository = getRepository(Account);

    const userInPost = authFactory.make(request);

    if (userInPost.email && userInPost.password) {

      const query = {
        where: {
          email: userInPost.email
        }
      };

      const userInDb = await repository.findOne(query);

      return authFactory.check(userInDb, userInPost, reply);

    } else {
      return reply({code:999,error:true,message:'erro ao realizar login'});
    }
  };

  async validateToken(request, decodedToken, callback) {

    const repository = getRepository(Account);

    const unserInDb = await repository.findOne(decodedToken.id,{
      relations:['listRoles']
    });

    let err = "";
    
    if(unserInDb) {
      return callback(err,true,unserInDb);
    } else {
      err = "usuário não encontrado";
      return callback(err,false,unserInDb);
    }
  }



  getToken() {
    return { texto: 'to aqui' }
  }

  removeToken() {

  }
}

export default AuthService;