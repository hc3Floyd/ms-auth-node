import supertest from 'supertest';
import chai from 'chai';
import app from '../../app';
//import { createConnection } from "typeorm";

global.Server = supertest(app.listener);
global.expect = chai.expect;
